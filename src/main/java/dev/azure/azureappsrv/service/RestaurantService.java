package dev.azure.azureappsrv.service;

import dev.azure.azureappsrv.entity.Restaurant;
import dev.azure.azureappsrv.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {

  @Autowired
  private RestaurantRepository restaurantRepository;
  public List<Restaurant> findAll(){
    return restaurantRepository.findAll();
  }
}
