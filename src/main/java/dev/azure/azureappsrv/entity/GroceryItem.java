package dev.azure.azureappsrv.entity;

import lombok.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Value
@Document("grocery_items")
public class GroceryItem {

  @Id
  String id;
  String name;
  int quantity;
  String category;

  public GroceryItem(String id, String name, int quantity, String category) {
    super();
    this.id = id;
    this.name = name;
    this.quantity = quantity;
    this.category = category;
  }
}
