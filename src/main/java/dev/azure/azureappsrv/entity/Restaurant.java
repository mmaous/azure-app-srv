package dev.azure.azureappsrv.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("restaurants")
public class Restaurant {

  @Id
  private String id;

  private Address address;
  private String borough;
  private String cuisine;
  private List<Grade> grades;
  private String name;
  private String restaurantId;
}
