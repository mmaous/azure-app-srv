package dev.azure.azureappsrv.entity;

import lombok.Data;
import java.util.Date;

@Data
public class Grade {
  private Date date;
  private String grade;
  private int score;
}
