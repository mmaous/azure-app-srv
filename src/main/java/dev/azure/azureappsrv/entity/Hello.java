package dev.azure.azureappsrv.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class Hello {
  private String message;
}