package dev.azure.azureappsrv.controller;

import dev.azure.azureappsrv.entity.Restaurant;
import dev.azure.azureappsrv.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/restaurant")
public class RestaurantController {
  @Autowired
  private RestaurantService restaurantService;
  @GetMapping("/all")
  private List<Restaurant> findAll(){
    return restaurantService.findAll();
  }
}
