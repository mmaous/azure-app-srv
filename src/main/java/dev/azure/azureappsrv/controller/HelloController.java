package dev.azure.azureappsrv.controller;

import dev.azure.azureappsrv.entity.Hello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

  @GetMapping
  public Hello helloBack() {
    return new Hello("Heyy, I'm up!!");
  }
}
