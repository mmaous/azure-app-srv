package dev.azure.azureappsrv.repository;

import dev.azure.azureappsrv.entity.GroceryItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface GroceryItemRepository extends MongoRepository<GroceryItem, String> {

  @Query("{'name':'?0'}")
  GroceryItem findGroceryItemByName(String name);

}

