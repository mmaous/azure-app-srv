package dev.azure.azureappsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzureAppSrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(AzureAppSrvApplication.class, args);
	}

}
