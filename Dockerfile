# Stage 1: Build stage
FROM maven:3-openjdk-17-slim AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

# Stage 2: Run stage
FROM openjdk:17-jdk-slim AS runtime
WORKDIR /app
COPY --from=build /app/target/azure-app-srv-0.0.1.jar /app/azure-app-srv-0.0.1.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/azure-app-srv-0.0.1.jar"]
